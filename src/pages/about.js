import React from 'react';
import Helmet from 'react-helmet';

import Layout from 'components/Layout';
import Container from 'components/Container';

const SecondPage = () => {
  return (
    <Layout pageName="about">
      <Helmet>
        <title>About Covid-19 Dashboard</title>
      </Helmet>
      <Container type="content" className="text-center">
        <h1>About Covid-19 Dashboard</h1>
        <p>
          Welcome to Covid-19 Dashboard. It is designed to show the numbers of cases, recoveries, deaths by country and
          the total in the world.
        </p>
        <p>
          It was developed with the API by{ ' ' }
          <a href="https://corona.lmao.ninja/" target="_blank" rel="noopener noreferrer">
            disease.sh - Open Disease Data
          </a>
          .
        </p>
      </Container>
    </Layout>
  );
};

export default SecondPage;
